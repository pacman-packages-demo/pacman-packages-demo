# archlinux-packages-demo

## Popular packages
* [site:archlinux.org/packages](https://google.com/search?q=site:archlinux.org/packages)
* [site:www.archlinux.org/packages](https://google.com/search?q=site:www.archlinux.org/packages)
* [site:aur.archlinux.org/packages](https://google.com/search?q=site:aur.archlinux.org/packages)

## Projects using multilib
* [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj/blob/master/.gitlab-ci.yml)

## Projects using AUR
* Also use useradd
* [pacman-packages-demo/mandoc
  ](https://gitlab.com/pacman-packages-demo/mandoc/blob/master/README.md)
  provides man instead of mman in Debian and others and is used by:
  * [pacman-packages-demo/ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
